
$(document).ready(function(){
    $('body').tooltip({
        selector: '[data-toggle=tooltip]'
    });
    $(".segment-select").Segment();


    $("#aq-team").val($("#aq-category option:first").data("teamid"));
    $('#aq-team').selectpicker('refresh'); 

    /*
        Datatable Definitions
    */
    var dbtTeams = $("#db-teams").DataTable({
        ajax: globalUrl + '/admin/teams/getdashboarddata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/teams/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>';
                }
            }, 
            { "data": "name" }, //2
            { "data": "categories" }, //3
            { "data": "questions" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [3,4],
                "render": function ( data, type, full, meta ) {
                    return data.length;
                }
            }
        ],
        deferRender: true,
        bPaginate: false,
        bLengthChange: false,
        bFilter: false,
        info: false,
        ordering: false
    });

    var ttTeams = $("#teamstable").DataTable({
        ajax: globalUrl + '/admin/teams/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/teams/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>';
                }
            },
            { "data": "name" }, //2
            { "data": "descr" }, //3
            { "data": "remarks" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            }
        ],
        deferRender: true
    });

    var itCategories = $("#categoriestable").DataTable({
        ajax: globalUrl + '/admin/categories/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/categories/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the category details and it\'s questions."><i class="ti-share"></a>';
                }
            },
            { "data": "team" }, //2
            { "data": "name" }, //2
            { "data": "descr" }, //3
            { "data": "remarks" }, //4
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            }
        ],
        deferRender: true
    });

    var itQuestions = $("#questionstable").DataTable({
        ajax: globalUrl + '/admin/questions/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/admin/questions/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the question details."><i class="ti-share"></a>';
                }
            },
            { "data": "team" }, //2
            { "data": "category" }, //2
            { "data": "name" }, //2
            { "data": "points" }, //3
            { "data": "guide" }, //3
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": 2,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            },
            {
                "targets": 3,
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            }
        ],
        deferRender: true
    });
    



    var dbsTeams = $("#dbstaff-teams").DataTable({
        ajax: globalUrl + '/staff/teams/getdashboarddata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/staff/teams/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view the team and it\'s contents."><i class="ti-share"></a>';
                }
            }, 
            { "data": "name" }, //2
            { "data": "transactions" }, //3
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [3],
                "render": function ( data, type, full, meta ) {
                    return data.length;
                }
            }
        ],
        deferRender: true,
        bPaginate: false,
        bLengthChange: false,
        bFilter: false,
        info: false,
        ordering: false
    });


    //Index for Staff Team
    var tfiTeam = $("#teamformsindex").DataTable({
        ajax: globalUrl + '/staff/'+globalCurrentTeam+'/getindexdata',
        columns:    [
            { "data": "id"},
            {
                "data": null,
                "render": function ( data, type, full, meta ) {
                    return '<a href="'+globalUrl+'/staff/'+data.id+'/details" data-toggle="tooltip" data-placement="top" title="Click to view this forms details."><i class="ti-share"></a>';
                }
            },
            { "data": "team" }, //3
            { "data": "fielddata" }, //4
            { "data": "user" }, //5
            { "data": "fielddata" }, //6
            { "data": "fielddata" }, //7
            { "data": "created_at" }, //8
            { "data": "sys_total" }, //9
        ],
        columnDefs: [
            {
                "targets": [ 0 ],
                "visible": false,
                "searchable": false
            },
            {
                "targets": [2,4],
                "render": function ( data, type, full, meta ) {
                        return data.name;
                }
            },
            {
                "targets": 3,
                "render": function ( data, type, full, meta ) {
                    //console.log(data[0], data.length);
                    for (var i = 0; i < data.length; i++) {
                        if(data[i].field.label.toLowerCase().indexOf("person") >= 0){
                            return data[i].value;
                        }
                    } //end for
                    return "Anon";      
                }
            },
            {
                "targets": 5,
                "render": function ( data, type, full, meta ) {
                    for (var i = 0; i < data.length; i++) {
                        if(data[i].field.label.toLowerCase().indexOf("transaction reference") >= 0){
                            return data[i].value;
                        }
                    } //end for
                    return "Septememver";      
                }
            },
            {
                "targets": 6,
                "render": function ( data, type, full, meta ) {
                    for (var i = 0; i < data.length; i++) {
                        if(data[i].field.label.toLowerCase().indexOf("transaction date") >= 0){
                            return data[i].value;
                        }
                    } //end for
                    return "Septememver";      
                }
            },
        ],
        deferRender: true,
        deferLoading: 25,
    });



    /*
        Datatable Events
    */
    $("#teamstable").on('click', '#at-mAddTeam', function(event) {
        $("#at-tablehost").val('teamstable');
    });

    $("#db-teams").on('click', '#at-sAddTeam', function(event) {
        $("#at-tablehost").val('db-teams');
    });

    $("#categoriestable").on('click', '#ac-mAddCategory', function(event) {
        $("#ac-tablehost").val('categoriestable');
    });

    $("body").on('click', '#std-mAddCategory', function(event) {
        teamid = $(this).data("teamid");
        teamname = $(this).data("teamname");

        $("#ac-tablehost").val('accordionTeamDetails');
        $('#ac-team').empty().append(new Option(teamname, teamid));
        $('#ac-team').selectpicker('refresh');
        $('#ac-team').val($('#ac-team option:first').val());

    });

    $('#fAddQuestion').on('changed.bs.select', '#aq-team', function (e) {
        $("#aq-category option").removeProp('disabled');
        $("#aq-category option").removeAttr('disabled');

        $("#aq-category option").each(function(index, el) {
            teamid = $(el).data("teamid");

            if (teamid != $("#aq-team option:selected").val()) {
                
                $(el).prop('disabled', "true");

                $('#aq-category').val($('#aq-category option:first').val());
                $('#aq-category').selectpicker('refresh');
                $('#aq-category').selectpicker('render');

            }
        });
    });

    $("#fAddQuestion").on('changed.bs.select', '#aq-category', function(event) {
        teamid = $(this).find("option:selected").data("teamid");

        $("#aq-team").val(teamid);
        $('#aq-team').selectpicker('refresh');
    });

    $("#detailswrapper").on('click', '#std-mAddQuestion', function(event) {
        let element = $(this);

        $('#aq-team').empty().append(new Option(element.data("teamname"), element.data("teamid")))
            .selectpicker('refresh')
            .val($('#aq-team option:first').val());
        $('#aq-category').empty().append(new Option(element.data("categoryname"), element.data("categoryid")))
            .selectpicker('refresh')
            .prop({"data-teamid": element.data("teamid")})
            .val($('#aq-category option:first').val());
    });

    $("body").on('click', '#scd-mAddQuestion', function(event) {
        let element = $(this);

        $('#aq-team').empty().append(new Option(element.data("teamname"), element.data("teamid")))
            .selectpicker('refresh')
            .val($('#aq-team option:first').val());
        $('#aq-category').empty().append(new Option(element.data("categoryname"), element.data("categoryid")))
            .selectpicker('refresh')
            .prop({"data-teamid": element.data("teamid")})
            .val($('#aq-category option:first').val());
    });

    $("#mAddField").on('change', 'input[type="radio"][name="optradio"]', function(event) {
         event.preventDefault();
         
         if (this.value == "text") {
            $("#twrapper").removeClass('hidden');
            $("#selectwrapper").addClass('hidden');

         }else if(this.value == "select"){
            $("#twrapper").removeAttr("class").addClass('hidden');
            $("#selectwrapper").removeClass('hidden');
         }else{
            $("#twrapper").addClass('hidden');
            $("#selectwrapper").addClass('hidden');
         }
     });
    
    $("#accordionGeneralInfo").on('click', '#af-mAddField', function(event) {
        $("#af-tablehost").val('tdtable_general');
        $("#af-teamid").val($(this).data('teamid'));
    });



    ttTeams.on('click', 'tbody tr td', function() {
            
            if (ttTeams.cell(this).index().column > 1) {
                let t = ttTeams.row(this).data();

                document.getElementById('fEditTeam').reset();
                $("#mEditTeam").modal('toggle');

                $("#et-teamid").val(t.id);
                $("#et-rowindex").val(ttTeams.row(this).index());
                $("#et-tablehost").val("teamstable");
                $("#et-name").val(t.name);
                $("#et-descr").val(t.descr);
                $("#et-remarks").val(t.remarks);
            }
            
        });

    itCategories.on('click', 'tbody tr td', function() {
            if (itCategories.cell(this).index().column > 1) {
                let t = itCategories.row(this).data();

                document.getElementById('fEditCategory').reset();
                $("#mEditCategory").modal('toggle');

                $("#ec-rowindex").val(itCategories.row(this).index());
                $("#ec-categoryid").val(t.id);
                
                $("#ec-team").empty().append(new Option(t.team.name, t.team.id));
                $("#ec-name").val(t.name);
                $("#ec-descr").val(t.descr);
                $("#ec-remarks").val(t.remarks);
            }
            
        });

    itQuestions.on('click', 'tbody tr td', function() {
            if (itQuestions.cell(this).index().column > 1) {
                let t = itQuestions.row(this).data();

                document.getElementById('fEditQuestion').reset();
                $("#mEditQuestion").modal('toggle');

                $("#eq-rowindex").val(itQuestions.row(this).index());
                $("#eq-questionid").val(t.id);
                
                $("#eq-team").empty().append(new Option(t.team.name, t.team.id));
                $("#eq-category").empty().append(new Option(t.category.name, t.category.id));
                $("#eq-name").val(t.name);
                $("#eq-points").val(t.points);
                $("#eq-guide").val(t.guide);
            }
            
        });

    




});
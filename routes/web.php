<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/logout' , 'Auth\LoginController@logout');

Route::get('/', function () {

    if (Auth::check()) {
		if (Auth::user()->sys_accesslevel == "1") {
			return redirect("/admin/dashboard");
		}else{
			return redirect("/staff/dashboard");
		}
	}else{
		return redirect("/login");
	}
});

Route::get('/home', function () {

    if (Auth::check()) {
		if (Auth::user()->sys_accesslevel == "1") {
			return redirect("/admin/dashboard");
		}else{
			return redirect("/staff/dashboard");
		}
	}else{
		return redirect("/login");
	}
});




Route::get('/admin/dashboard', 'DashboardController@adminDashboard');
Route::get('/admin/teams/getdashboarddata', 'TeamController@dashboardData');
Route::post('/admin/teams/create', 'TeamController@create'); // for Dashboard

Route::get('/admin/teams', 'TeamController@index');
Route::get('/admin/teams/getindexdata', 'TeamController@indexData');
Route::patch('/admin/teams/update/{team}', 'TeamController@update');
Route::get('/admin/teams/{team}/details', 'TeamController@showTeamDetails');

Route::get('/admin/categories', 'CategoryController@index');
Route::get('/admin/categories/getindexdata', 'CategoryController@indexData');
Route::post('/admin/categories/create', 'CategoryController@create'); // for Dashboard
Route::patch('/admin/categories/update/{category}', 'CategoryController@update');
Route::get('/admin/categories/{category}/details', 'CategoryController@showCategoryDetails');

Route::get('/admin/questions', 'QuestionController@index');
Route::get('/admin/questions/getindexdata', 'QuestionController@indexData');
Route::post('/admin/questions/create', 'QuestionController@create'); // for Dashboard
Route::patch('/admin/questions/update/{question}', 'QuestionController@update');

Route::post('/admin/fields/create', 'FieldController@create'); // for Dashboard


Route::get('/staff/dashboard', 'DashboardController@staffDashboard');
Route::get('/staff/teams/getdashboarddata', 'StaffTeamController@dashboardDataStaff');

Route::get('/staff/{team}/index', 'StaffTeamController@index');
Route::get('/staff/{team}/getindexdata', 'StaffTeamController@indexData');
Route::get('/staff/{team}/form', 'StaffFormController@showForm');
Route::post('/staff/{team}/save', 'StaffFormController@saveForm');
Route::get('/staff/{transaction}/details', 'StaffFormController@showFormDetails');
Route::post('/staff/{transaction}/savedetails', 'StaffFormController@updateFormDetails');
Route::get("/staff/{team}/guide", "StaffFormController@guideindex");
Route::get('/staff/{team}/history', 'StaffHistoryController@index');
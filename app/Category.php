<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $guarded = array();

    public function team (){
        return $this->belongsTo(Team::class)->select("id", "name");
    }

    public function questions (){
        return $this->hasMany(Question::class, "category_id", "id");
    }
}

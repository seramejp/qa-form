<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;

class StaffHistoryController extends Controller
{
    /**
     * Controller for Admin
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }



    /**
     * Form View for Staff - blank form
     *
     * @return Collection $fields
     */
    public function index (Team $team){

    	$teams = Team::all();

        return view('staff.history.index', compact('team', 'teams'));   
    }
}

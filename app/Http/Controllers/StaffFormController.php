<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
use App\Field;
use App\Fielddata;
use App\Transaction;
use App\Question;
use App\Answer;

use Auth;

class StaffFormController extends Controller
{
    /**
     * Controller for Teams
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Form View for Staff - blank form
     *
     * @return Collection $fields
     */
    public function showForm (Team $team){

    	$teams = Team::all();
    	$fields = Field::where("team_id", $team->id)->orderBy("order")->get();

        return view('staff.forms.form', compact('team', 'teams', 'fields'));   
    }


    /**
     * Form View for Staff
     *
     * @return Collection $fields
     */
    public function saveForm (Request $request, Team $team){
        //dd($request->all());

        // Save Transaction
        $trans = new Transaction;
        $trans->team_id = $team->id;
        $trans->user_id = Auth::user()->id;
        $trans->remarks = $request->remarks;
        $trans->sys_total = 0;
        $trans->save();


        foreach($request->all() as $key => $value){
            
            if (strpos($key, "field") !== false) {
                $fieldId = explode("fielddata_", $key)[1] ? intval(explode("fielddata_", $key)[1]) : 0;

                $newFieldData = new Fielddata;
                $newFieldData->transaction_id = $trans->id;
                $newFieldData->field_id = $fieldId;
                $newFieldData->value = $value;
                $newFieldData->save();
                
            }elseif(strpos($key, "question") !== false ){
                $questionId = explode("question_", $key)[1] ? intval(explode("question_", $key)[1]) : 0;

                $theQuestion = Question::find($questionId);

                if (!is_null($theQuestion)) {
                    $newAnswer = new Answer;
                    $newAnswer->user_id = Auth::user()->id;
                    $newAnswer->transaction_id = $trans->id;
                    $newAnswer->question_id = $questionId;
                    $newAnswer->score = ($value != "no" ? intval($theQuestion->points) : 0);
                    $newAnswer->theanswer = $value;
                    $newAnswer->save();

                    $trans->sys_total += ($value != "no" ? intval($theQuestion->points) : 0);
                    $trans->save();
                }

            }

        }
        return redirect('/staff/'.$team->id.'/index');
    } 


    /**
     * Form View for Staff - for update/view
     *
     * @return Collection $fields
     */
    public function showFormDetails (Transaction $transaction){
        $team = Team::find($transaction->team_id);
        $teams = Team::all();
        $fields = Field::where("team_id", $transaction->team_id)->orderBy("order")->get();

        if ($transaction->answers->count() > 0) {
            return view('staff.forms.show', compact('team', 'teams', 'fields', 'transaction'));   
        }else{
            return view('staff.forms.form', compact('team', 'teams', 'fields'));   
        }
        
    }


    /**
     * Form View for Staff
     *
     * @return Collection $fields
     */
    public function updateFormDetails (Request $request, Transaction $transaction){
        $transaction->remarks = $request->remarks;
        $transaction->sys_total = 0;
        $transaction->save();

        foreach($request->all() as $key => $value){
            
            if (strpos($key, "field") !== false) {
                $fielddataId = explode("fielddata_", $key)[1] ? intval(explode("fielddata_", $key)[1]) : 0;

                $theData = Fielddata::find($fielddataId);
                $theData->value = $value;
                $theData->save();
                
            }elseif(strpos($key, "answer") !== false ){
                $answerId = explode("answer_", $key)[1] ? intval(explode("answer_", $key)[1]) : 0;

                $usersAnswer = Answer::find($answerId);

                if (!is_null($usersAnswer)) {
                    $usersAnswer->theanswer = $value;
                    $usersAnswer->score =  ($value != "no" ? intval($usersAnswer->question->points) : 0);
                    $usersAnswer->save();

                    $transaction->sys_total += ($value != "no" ? intval($usersAnswer->question->points) : 0);
                    $transaction->save();
                }

            }

        }

        return redirect("/staff/" . $transaction->team_id . "/index");
    }



    /**
     * Load Guide
     *
     * @return void
     */
    public function guideindex (Team $team){
        $teams = Team::all();
        
        return view("staff.guides.index", compact('team', 'teams'));
    } 



}


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
use App\Transaction;
use Auth;

class StaffTeamController extends Controller
{
    

	/**
     * Controller for Teams
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Index for Teams - STAFF
     *
     * @return Collection $teams
     */
    public function index (Team $team){
    	$teams = Team::all();
        return view('staff.teams.index', compact('team', 'teams'));   
    }


    /**
     * Dashboard for Teams - Staff
     *
     * @return Collection $teams
     */
    public function dashboardDataStaff (){
        $teams = Team::select("id", "name")->with("transactions")->orderBy("name")->get();

        return response()->json([
            'data' => $teams,        
        ]);
    }


    /**
     * Dashboard for Teams - Staff
     *
     * @return Collection $teams
     */
    public function indexData (Team $team){
        $transactions = Transaction::where("team_id", $team->id)
        	->with('team', 'user', 'fielddata.field')
        	->get();

        return response()->json([
            'data' => $transactions,        
        ]);
    }



    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Team;
use App\Category;
use App\Question;
use Auth;

class QuestionController extends Controller
{
    /**
     * Controller for questions
     *
     * @return void
     */

    public function __construct(){
        $this->middleware('auth');
    }


    /**
     * Index for questions
     *
     * @return View questions.index
     */
    public function index (){
    	$teams = Team::select("id", "name")->orderBy("name")->get();
    	$categories = Category::select("id", "name", "team_id")->orderBy("name")->get();
        return view('admin.questions.index', compact('teams', 'categories'));   
    }


    /**
     * Index for questions
     *
     * @return Collection $questions
     */
    public function indexData (){
        $questions = Question::select('id', 'name', 'points', 'guide', 'category_id', 'team_id', 'user_id')->with('category', 'team')->orderBy("name")->get();
       	
        return response()->json([
            'data' => $questions,        
        ]);
    }


    /**
     * Create Question
     *
     * @return void
     */
    public function create (Request $request){
        $newQuestion = new Question;
        $newQuestion->name = $request->name;
        $newQuestion->guide = $request->guide;
        $newQuestion->points = $request->points;
        $newQuestion->sys_isactive = "1";
        $newQuestion->user_id = Auth::user()->id;
        $newQuestion->team_id = $request->teamid;
        $newQuestion->category_id = $request->categoryid;
        $newQuestion->save();

        return $newQuestion->id;   
    }


    /**
     * Update for Question
     *
     * @return void
     */
    public function update (Question $question, Request $request){
        $question->name = $request->name;
        $question->points = $request->points;
        $question->guide = $request->guide;
        $question->save();
    } 
}

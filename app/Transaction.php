<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    public function team (){
        return $this->belongsTo(Team::class)->select("id", "name");
    }

    public function user (){
        return $this->belongsTo(User::class)->select("id", "name");
    }

    public function fieldperson (){
        return $this->hasMany(Fielddata::class, "transaction_id", "id")
        	->whereHas("person", function($query){
        		$query->where("label", "person");
        	});
    }   

 	public function fielddata (){
        return $this->hasMany(Fielddata::class, "transaction_id", "id");
    } 

    public function answers (){
        return $this->hasMany(Answer::class, "transaction_id", "id");
    } 



}

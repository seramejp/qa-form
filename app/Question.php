<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $guarded = array();

    public function team (){
        return $this->belongsTo(Team::class)->select("id", "name");
    }

    public function category (){
        return $this->belongsTo(Category::class)->select("id", "name", "team_id");
    }
}

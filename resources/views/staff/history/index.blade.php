@extends('layouts.app')

@section('main-content')
	<h3 class="text-white">{{$team->name}}</h3>
	<h5 class="text-white">Their past, your history. This list contains the people you've judged.</h5>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            
			<div class="row animate-box">
            
	            <table class="table table-hover" cellspacing="0" width="100%" id="formshistory">
	                <caption><div id="buttoncontainer"></div></caption>
	                <thead>
	                    <tr>
	                        <th width="4%"></th>
	                        <th>Encoded on</th>
	                        <th>Who?</th>
	                        <th>Trans Ref</th>
	                        <th>Trans Date</th>
	                        <th>Total Score</th>
	                        <th>Remarks</th>
	                    </tr>
	                </thead>


	                    <tfoot>
	                        
	                    </tfoot>

	                <meta name="csrf-token" content="{{ csrf_token() }}" />
	                
	                <tbody>
	                </tbody>
	            </table>
	    	</div>
    </div>

@endsection

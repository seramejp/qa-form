@extends('layouts.app')


@section('main-content')
    <h3 class="text-white">{{$team->name}}</h3>
    <h5 class="text-white">The Form - Judging the person with your cold heart. </h5>
    <hr>

@endsection

@section("below-main-content")
    <form id="form-fields" action="{{url('/staff/'.$team->id.'/save')}}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}

            @if(count($fields) > 0)
                
                    <div class="panel">
                        <div class="panel-heading" style="background: linear-gradient(to right, #3dcdbc 0%, #33a1bd 100%); color: white;">
                            <h4 class="panel-title">General Information</h4>
                        </div>
                        
                        <div class="panel-body">
                            <div class="col-lg-10 col-lg-offset-1">
                                @foreach($fields as $field)  
                                    <div class="form-group">
                                        <label for="">{{$field->label}}</label>
                                            @if($field->type == "text" || $field->type == "date")             
                                                <input type="{{$field->type}}" name="fielddata_{{$field->id}}" class="form-control">
                                            @else
                                                <select name="fielddata_{{$field->id}}" class="form-control">
                                                    @php
                                                        $options = explode(";", $field->options);
                                                        foreach ($options as $value) {
                                                            if (trim($value) != "") {
                                                                echo '<option value="'.$value.'">'.$value.'</option>';
                                                            }
                                                        }
                                                    @endphp
                                                </select>
                                            @endif
                                    </div>
                                @endforeach

                                <div class="form-group">
                                    <label for="">Remarks</label>
                                    <textarea name="remarks" cols="15" rows="3" placeholder="something to say about the matter?" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
            @endif

            @if(count($team->categories) > 0)
                @foreach($team->categories as $cat)
                    
                        @if( count($cat->questions) > 0)
                            <h3 style="padding-top: 20px;"><i class="ti-light-bulb"></i> {{ ucfirst($cat->name) }}</h3>
                            
                            
                            @foreach($cat->questions as $ask)
                                @if(is_null($ask->deleted_at))
                                    <div class="row animate-box">
                                        <div class="service">
                                            <div class="col-md-7 col-md-offset-1">
                                                <p>{{ $ask->name }}</p>
                                            </div>

                                            <div class="col-md-3">
                                                <select class="segment-select" name="question_{{$ask->id}}">
                                                    <option value="na" selected="">N/A</option>
                                                    <option value="yes">Yes</option>
                                                    <option value="no">No</option>
                                                </select>
                                            </div>

                                        </div>
                                    </div>
                                @endif

                            @endforeach

                        @endif

                @endforeach
                <hr>
                <div class="row animate-box">
                    <div class="service">
                        <div id="form-btn-design" class="pull-right" style="">
                        
                            <a role="button" type="button" class="btn btn-outline" href="{{url('/staff/'.$team->id.'/form')}}"><i class="ti-trash"></i> Clear</a>

                            <button type="submit" class="btn btn-success btn-outline" style="border-color:green;"><i class="ti-save"></i> Save</button>
                        </div>
                    </div>
                </div>

            @endif
    </form>
@endsection





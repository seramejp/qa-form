<!DOCTYPE html>
<html lang="en">

@include ('layouts.head')

<body>
    <div class="gtco-loader"></div>

    <div id="app">
        @if (Auth::check())
        
                    @include ('layouts.nav')

                    @if(Auth::user()->sys_accesslevel == "1")
                        @include('admin.headermenu')
                    @else
                        @include('staff.headermenu')
                    @endif

                    <header id="gtco-header" class="gtco-cover" role="banner">
                        <div class="gtco-container">
                            <div class="row">
                                <div class="col-md-12 col-md-offset-0 text-left">
                                    <div class="display-t">
                                        <div class="display-tc">
                                            <br>
                                            
                                            @yield('main-content')
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <!-- END #gtco-header -->

                    <div class="gtco-section">
                        <div class="gtco-container">

                            @yield('below-main-content')        

                        </div>
                    </div>
                    


                    @include('layouts.footer')

                    
        @else
            @yield('login-content')
        @endif
    
    </div>

    @include ('layouts.requiredjs')

</body>
</html>

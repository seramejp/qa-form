@extends('layouts.app')

@section('main-content')
	<ol class="breadcrumb" style="background: transparent !important;">
		<li class="text-white">
			<img src="{{url('/images/categories-32.png')}}" alt=""> Categories
		</li>

		<li class="active">index</li>
	    <li><a id="ac-mAddCategory" href="" data-toggle='modal' data-target="#mAddCategory">create</a></li>
	</ol>
@endsection

@section('below-main-content')
	<div class="row animate-box">
            
			<table class="table table-hover" id="categoriestable" cellspacing="0" width="100%">
				<thead>
					<tr>
						<th>ID</th>
						<th width="4%"></th>
						<th>Team Name</th>
						<th>Category Name</th>
						<th>Description</th>
						<th>Remarks</th>
					</tr>
				</thead>
				<tfoot>
					<th colspan="6" rowspan="1">
						<a id="ac-mAddCategory" href="" data-toggle='modal' data-target="#mAddCategory" class="btn btn btn-special"><i class="ti-plus"></i> New Category</a>
					</th>
				</tfoot>
				
				<tbody>
				</tbody>
			</table>
    </div>

    @include('admin.modals.addcategory')
    @include('admin.modals.editcategory')
@endsection

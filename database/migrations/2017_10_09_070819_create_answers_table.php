<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnswersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answers', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->integer('user_id')->unsigned()->default(1); //author of the team
            $table->integer('transaction_id')->unsigned()->default(0); //author of the team
            $table->integer('question_id')->unsigned()->default(0); //author of the team

            $table->integer('score')->default(0); //author of the team
            $table->string('theanswer'); //author of the team
            
            $table->timestamps();
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('answers');   
    }
}

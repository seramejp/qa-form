<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCategories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->engine = 'InnoDB';

            $table->increments('id');
            $table->string('name'); //team name
            $table->text('descr')->nullable(); // description of this team
            $table->text('remarks')->nullable(); // remarks, notes, etc.
            $table->integer('user_id')->unsigned()->default(1); //author of the team
            $table->integer('team_id')->unsigned()->default(1); //which team
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
}
